NM questions about the CLI/Mono team work

P&P

 MP1. A third party indicates that a package you maintain may infringe one or
      more patents. How do you proceed?
 MP2. It is often claimed that Mono is a patent risk. In your own words, can
      you summarise the situation and Debian's position?
 MP3. Can you explain the acronym/term(s) CLI/CLR/BCL/.NET/CIL? (select a
      couple)
 MP4. What would you do if you discovered that an upstream of a package you
      maintain contains binary blobs? If these binary blobs don't violate the
      distribution license, are they acceptable for inclusion in Debian main?

T&S

 MT1. How can you tell whether a CLI package is ABI compatible with previous
      versions? Are there any tools that can help you with this? Can you do
      anything besides patching the source's ABI version if an ABI bump is
      incorrect?
 MT2. What is a DLL map and when might you need one?
 MT2a. An application ships a DLL Map <dllmap dll="foo" target="libfoo.so.0">.
       Why could this be a problem (consider transitions)? What can be done to
       minimise the risk of such mistakes? What if the target was libfoo.so
       instead?
 MT2b. When might it be appropriate to use the --exclude-moduleref option to
       dh_clideps?
 MT3. What packaging helpers are available when packaging CLI libraries?

